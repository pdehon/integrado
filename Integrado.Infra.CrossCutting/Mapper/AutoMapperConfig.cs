﻿using AutoMapper;
using Integrado.Application.Homes;
using Integrado.Application.Microregions;
using Integrado.Application.Persons;
using Integrado.Application.Questions;

namespace Integrado.Infra.CrossCutting.Mapper
{
    public static class AutoMapperConfig
    {
        public static Type[] Setup()
        {
            var profiles = RegisterMappings();
            return profiles.Select(c => c.GetType()).Distinct().ToArray();
        }

        public static IEnumerable<Profile> RegisterMappings()
        {
            yield return new HomeMappingProfile();
            yield return new PersonMappingProfile();
            yield return new MicroregionMappingProfile();
            yield return new QuestionMappingProfile();
        }
    }
}
