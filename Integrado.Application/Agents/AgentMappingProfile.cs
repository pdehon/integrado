﻿using AutoMapper;
using Integrado.Application.Agents.Responses;
using Integrado.Domain.Models;

namespace Integrado.Application.Agents
{
    public class HealthCenterMappingProfile : Profile
    {
        public HealthCenterMappingProfile()
        {
            CreateMap<Agent, AgentResponse>();
            CreateMap<HealthCenter, AgentHealthCenterResponse>();
            CreateMap<Microregion, AgentMicroregionResponse>();
        }
    }
}
