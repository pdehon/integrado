﻿using AutoMapper;
using Integrado.Domain.Models;

namespace Integrado.Application.Questions
{
    public class QuestionMappingProfile : Profile
    {
        public QuestionMappingProfile()
        {
            CreateMap<QuestionListQuery, Question>();

            CreateMap<Question, QuestionResponse>();

            CreateMap<OtherQuestion, OtherQuestionResponse>();
        }
    }
}
