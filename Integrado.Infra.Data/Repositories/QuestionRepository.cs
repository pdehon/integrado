﻿using Integrado.Domain.Interfaces.Data;
using Integrado.Domain.Models;
using Integrado.Infra.Data.Context;

namespace Integrado.Infra.Data.Repositories
{
    public class QuestionRepository : Repository<Question>, IQuestionRepository
    {
        public QuestionRepository(ApplicationContext context)
            : base(context)
        {
        }
    }
}
