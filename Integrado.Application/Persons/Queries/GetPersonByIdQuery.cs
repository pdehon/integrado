﻿using Integrado.Application.Persons.Responses;
using Integrado.Domain.Core.Messaging;

namespace Integrado.Application.Persons.Queries
{
    public class GetPersonByIdQuery : Query<PersonResponse?>
    {
        public Guid Id { get; }

        public GetPersonByIdQuery(Guid id) => Id = id;
    }
}
