﻿using Integrado.Domain.Core.Messaging;

namespace Integrado.Application.Questions
{
    public class QuestionListQuery : Query<IEnumerable<QuestionResponse>>
    {
    }
}
