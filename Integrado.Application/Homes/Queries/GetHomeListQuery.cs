﻿using Integrado.Application.Homes.Responses;
using Integrado.Domain.Core.Messaging;

namespace Integrado.Application.Homes.Queries
{
    public class GetHomeListQuery : QueryList<IEnumerable<HomeResponse>>
    {
        public Guid MicroregionId { get; set; }
    }
}
