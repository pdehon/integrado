﻿using Integrado.Application.Agents.Responses;
using Integrado.Domain.Core.Messaging;

namespace Integrado.Application.Agents.Queries
{
    public class GetAgentByUserIdQuery : Query<AgentResponse>
    {
        public Guid UserId { get; set; }

        public GetAgentByUserIdQuery(Guid userId)
        {
            UserId = userId;
        }
    }
}
