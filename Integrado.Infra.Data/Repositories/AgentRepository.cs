﻿using Integrado.Domain.Interfaces.Data;
using Integrado.Domain.Models;
using Integrado.Infra.Data.Context;

namespace Integrado.Infra.Data.Repositories
{
    public class AgentRepository : Repository<Agent>, IAgentRepository
    {
        public AgentRepository(ApplicationContext context)
            : base(context)
        {
        }
    }
}
