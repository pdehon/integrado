﻿using Integrado.Domain.Interfaces.Data;
using Integrado.Domain.Models;
using Integrado.Infra.Data.Context;

namespace Integrado.Infra.Data.Repositories
{
    public class PersonRepository : Repository<Person>, IPersonRepository
    {
        public PersonRepository(ApplicationContext context)
            : base(context)
        {
        }
    }
}
