﻿using Integrado.Domain.Interfaces.Data;
using Integrado.Domain.Models;
using Integrado.Infra.Data.Context;

namespace Integrado.Infra.Data.Repositories
{
    public class UserPermissionRepository : Repository<UserPermission>, IUserPermissionRepository
    {
        public UserPermissionRepository(ApplicationContext context)
            : base(context)
        {
        }
    }
}
