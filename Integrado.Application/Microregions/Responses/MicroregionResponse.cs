﻿namespace Integrado.Application.Microregions.Responses
{
    public class MicroregionResponse
    {
        public Guid Id { get; set; }
        public string? Name { get; set; }
    }
}
