﻿// <auto-generated />
using System;
using Integrado.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

#nullable disable

namespace Integrado.Infra.Data.Migrations
{
    [DbContext(typeof(ApplicationContext))]
    [Migration("20230719040303_BaseStructure")]
    partial class BaseStructure
    {
        /// <inheritdoc />
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.8")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder);

            modelBuilder.Entity("Integrado.Domain.Models.Agent", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<DateTime>("AddedDate")
                        .HasColumnType("datetime2");

                    b.Property<bool>("Ativo")
                        .HasColumnType("bit");

                    b.Property<Guid>("HealthCenterId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid>("MicroregionId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<DateTime>("ModifiedDate")
                        .HasColumnType("datetime2");

                    b.Property<Guid>("UserId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("Id");

                    b.HasIndex("HealthCenterId");

                    b.HasIndex("MicroregionId");

                    b.HasIndex("UserId");

                    b.ToTable("Agent", (string)null);
                });

            modelBuilder.Entity("Integrado.Domain.Models.ChecklistQuestion", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<DateTime>("AddedDate")
                        .HasColumnType("datetime2");

                    b.Property<bool>("Ativo")
                        .HasColumnType("bit");

                    b.Property<DateTime>("ModifiedDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("varchar(200)");

                    b.Property<Guid>("QuestionId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("Id");

                    b.HasIndex("QuestionId");

                    b.ToTable("ChecklistQuestion", (string)null);
                });

            modelBuilder.Entity("Integrado.Domain.Models.HealthCenter", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid>("AccountableId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<DateTime>("AddedDate")
                        .HasColumnType("datetime2");

                    b.Property<bool>("Ativo")
                        .HasColumnType("bit");

                    b.Property<string>("Cnes")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.Property<DateTime>("ModifiedDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("varchar(100)");

                    b.HasKey("Id");

                    b.HasIndex("AccountableId")
                        .IsUnique();

                    b.ToTable("HealthCenter", (string)null);
                });

            modelBuilder.Entity("Integrado.Domain.Models.Home", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<DateTime>("AddedDate")
                        .HasColumnType("datetime2");

                    b.Property<bool>("Ativo")
                        .HasColumnType("bit");

                    b.Property<string>("ContactNumber")
                        .IsRequired()
                        .HasColumnType("varchar(15)");

                    b.Property<decimal>("HouseholdIncome")
                        .HasColumnType("decimal(10,2)");

                    b.Property<string>("MedicalRecordNumber")
                        .HasColumnType("varchar(50)");

                    b.Property<Guid>("MicroregionId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<DateTime>("ModifiedDate")
                        .HasColumnType("datetime2");

                    b.Property<decimal>("NumberMembers")
                        .HasColumnType("numeric(3)");

                    b.Property<Guid>("ResponsiblePersonId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("TypeProperty")
                        .IsRequired()
                        .HasColumnType("varchar(30)");

                    b.HasKey("Id");

                    b.HasIndex("MicroregionId");

                    b.HasIndex("ResponsiblePersonId")
                        .IsUnique();

                    b.ToTable("Home", (string)null);
                });

            modelBuilder.Entity("Integrado.Domain.Models.Microregion", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<DateTime>("AddedDate")
                        .HasColumnType("datetime2");

                    b.Property<bool>("Ativo")
                        .HasColumnType("bit");

                    b.Property<Guid>("HealthCenterId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<DateTime>("ModifiedDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.HasKey("Id");

                    b.HasIndex("HealthCenterId");

                    b.ToTable("Microregion", (string)null);
                });

            modelBuilder.Entity("Integrado.Domain.Models.Permission", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<DateTime>("AddedDate")
                        .HasColumnType("datetime2");

                    b.Property<bool>("Ativo")
                        .HasColumnType("bit");

                    b.Property<DateTime>("ModifiedDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("varchar(20)");

                    b.HasKey("Id");

                    b.ToTable("Permission", (string)null);
                });

            modelBuilder.Entity("Integrado.Domain.Models.Person", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<DateTime>("AddedDate")
                        .HasColumnType("datetime2");

                    b.Property<bool>("Ativo")
                        .HasColumnType("bit");

                    b.Property<DateTime>("BirthDate")
                        .HasColumnType("Date");

                    b.Property<string>("ContactNumber")
                        .IsRequired()
                        .HasColumnType("varchar(20)");

                    b.Property<string>("Document")
                        .IsRequired()
                        .HasColumnType("varchar(14)");

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasColumnType("varchar(15)");

                    b.Property<string>("FatherName")
                        .IsRequired()
                        .HasColumnType("varchar(100)");

                    b.Property<Guid>("MicroregionId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<DateTime>("ModifiedDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("MotherName")
                        .IsRequired()
                        .HasColumnType("varchar(100)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("varchar(100)");

                    b.Property<string>("NationalHealthRegister")
                        .IsRequired()
                        .HasColumnType("varchar(20)");

                    b.Property<string>("Nationality")
                        .IsRequired()
                        .HasColumnType("varchar(20)");

                    b.Property<string>("Sex")
                        .IsRequired()
                        .HasColumnType("varchar(20)");

                    b.Property<string>("SkinColor")
                        .IsRequired()
                        .HasColumnType("varchar(20)");

                    b.Property<string>("SocialIdentification")
                        .IsRequired()
                        .HasColumnType("varchar(20)");

                    b.Property<string>("SocialName")
                        .IsRequired()
                        .HasColumnType("varchar(100)");

                    b.HasKey("Id");

                    b.HasIndex("MicroregionId");

                    b.ToTable("Person", (string)null);
                });

            modelBuilder.Entity("Integrado.Domain.Models.Question", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<DateTime>("AddedDate")
                        .HasColumnType("datetime2");

                    b.Property<bool>("Ativo")
                        .HasColumnType("bit");

                    b.Property<bool>("Mandatory")
                        .HasColumnType("bit");

                    b.Property<DateTime>("ModifiedDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("varchar(200)");

                    b.Property<decimal>("ResponseType")
                        .HasColumnType("numeric(2,0)");

                    b.HasKey("Id");

                    b.ToTable("Question", (string)null);
                });

            modelBuilder.Entity("Integrado.Domain.Models.QuestionAnswer", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<DateTime>("AddedDate")
                        .HasColumnType("datetime2");

                    b.Property<bool>("Ativo")
                        .HasColumnType("bit");

                    b.Property<DateTime>("ModifiedDate")
                        .HasColumnType("datetime2");

                    b.Property<Guid>("QuestionId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Response")
                        .HasColumnType("varchar(200)");

                    b.HasKey("Id");

                    b.HasIndex("QuestionId");

                    b.ToTable("QuestionAnswer", (string)null);
                });

            modelBuilder.Entity("Integrado.Domain.Models.User", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<DateTime>("AddedDate")
                        .HasColumnType("datetime2");

                    b.Property<bool>("Ativo")
                        .HasColumnType("bit");

                    b.Property<string>("Cns")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ContactNumber")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.Property<DateTime>("ModifiedDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("varchar(200)");

                    b.Property<string>("PasswordHash")
                        .IsRequired()
                        .HasColumnType("nvarchar(256)");

                    b.HasKey("Id");

                    b.ToTable("User", (string)null);
                });

            modelBuilder.Entity("Integrado.Domain.Models.UserPermission", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<DateTime>("AddedDate")
                        .HasColumnType("datetime2");

                    b.Property<bool>("Ativo")
                        .HasColumnType("bit");

                    b.Property<DateTime>("ModifiedDate")
                        .HasColumnType("datetime2");

                    b.Property<Guid>("PermissionId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid>("UserId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("Id");

                    b.HasIndex("PermissionId");

                    b.HasIndex("UserId");

                    b.ToTable("UserPermission", (string)null);
                });

            modelBuilder.Entity("Integrado.Domain.Models.Agent", b =>
                {
                    b.HasOne("Integrado.Domain.Models.HealthCenter", "HealthCenter")
                        .WithMany("Agents")
                        .HasForeignKey("HealthCenterId")
                        .OnDelete(DeleteBehavior.Restrict)
                        .IsRequired();

                    b.HasOne("Integrado.Domain.Models.Microregion", "Microregion")
                        .WithMany("Agents")
                        .HasForeignKey("MicroregionId")
                        .OnDelete(DeleteBehavior.Restrict)
                        .IsRequired();

                    b.HasOne("Integrado.Domain.Models.User", "User")
                        .WithMany("Agents")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Restrict)
                        .IsRequired();

                    b.Navigation("HealthCenter");

                    b.Navigation("Microregion");

                    b.Navigation("User");
                });

            modelBuilder.Entity("Integrado.Domain.Models.ChecklistQuestion", b =>
                {
                    b.HasOne("Integrado.Domain.Models.Question", "Question")
                        .WithMany("ChecklistQuestions")
                        .HasForeignKey("QuestionId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Question");
                });

            modelBuilder.Entity("Integrado.Domain.Models.HealthCenter", b =>
                {
                    b.HasOne("Integrado.Domain.Models.User", "Accountable")
                        .WithOne("HealthCenter")
                        .HasForeignKey("Integrado.Domain.Models.HealthCenter", "AccountableId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.OwnsOne("Integrado.Domain.ValueObjects.Address", "Address", b1 =>
                        {
                            b1.Property<Guid>("HealthCenterId")
                                .HasColumnType("uniqueidentifier");

                            b1.Property<string>("City")
                                .IsRequired()
                                .HasColumnType("varchar(50)")
                                .HasColumnName("City");

                            b1.Property<string>("Landmark")
                                .HasColumnType("varchar(200)")
                                .HasColumnName("Landmark");

                            b1.Property<string>("Neighborhood")
                                .IsRequired()
                                .HasColumnType("varchar(50)")
                                .HasColumnName("Neighborhood");

                            b1.Property<decimal>("Number")
                                .HasColumnType("numeric(4,3)")
                                .HasColumnName("Number");

                            b1.Property<string>("PostalCode")
                                .IsRequired()
                                .HasColumnType("varchar(8)")
                                .HasColumnName("PostalCode");

                            b1.Property<string>("State")
                                .IsRequired()
                                .HasColumnType("varchar(50)")
                                .HasColumnName("State");

                            b1.Property<string>("Street")
                                .IsRequired()
                                .HasColumnType("varchar(100)")
                                .HasColumnName("Street");

                            b1.Property<string>("StreetComplement")
                                .HasColumnType("varchar(20)")
                                .HasColumnName("StreetComplement");

                            b1.HasKey("HealthCenterId");

                            b1.ToTable("HealthCenter");

                            b1.WithOwner()
                                .HasForeignKey("HealthCenterId");
                        });

                    b.Navigation("Accountable");

                    b.Navigation("Address")
                        .IsRequired();
                });

            modelBuilder.Entity("Integrado.Domain.Models.Home", b =>
                {
                    b.HasOne("Integrado.Domain.Models.Microregion", "Microregion")
                        .WithMany("Homes")
                        .HasForeignKey("MicroregionId")
                        .OnDelete(DeleteBehavior.Restrict)
                        .IsRequired();

                    b.HasOne("Integrado.Domain.Models.Person", "Person")
                        .WithOne("Home")
                        .HasForeignKey("Integrado.Domain.Models.Home", "ResponsiblePersonId")
                        .OnDelete(DeleteBehavior.Restrict)
                        .IsRequired();

                    b.OwnsOne("Integrado.Domain.ValueObjects.Address", "Address", b1 =>
                        {
                            b1.Property<Guid>("HomeId")
                                .HasColumnType("uniqueidentifier");

                            b1.Property<string>("City")
                                .IsRequired()
                                .HasColumnType("varchar(50)")
                                .HasColumnName("City");

                            b1.Property<string>("Landmark")
                                .HasColumnType("varchar(200)")
                                .HasColumnName("Landmark");

                            b1.Property<string>("Neighborhood")
                                .IsRequired()
                                .HasColumnType("varchar(50)")
                                .HasColumnName("Neighborhood");

                            b1.Property<decimal>("Number")
                                .HasColumnType("numeric(4,3)")
                                .HasColumnName("Number");

                            b1.Property<string>("PostalCode")
                                .IsRequired()
                                .HasColumnType("varchar(8)")
                                .HasColumnName("PostalCode");

                            b1.Property<string>("State")
                                .IsRequired()
                                .HasColumnType("varchar(50)")
                                .HasColumnName("State");

                            b1.Property<string>("Street")
                                .IsRequired()
                                .HasColumnType("varchar(100)")
                                .HasColumnName("Street");

                            b1.Property<string>("StreetComplement")
                                .HasColumnType("varchar(20)")
                                .HasColumnName("StreetComplement");

                            b1.HasKey("HomeId");

                            b1.ToTable("Home");

                            b1.WithOwner()
                                .HasForeignKey("HomeId");
                        });

                    b.Navigation("Address")
                        .IsRequired();

                    b.Navigation("Microregion");

                    b.Navigation("Person");
                });

            modelBuilder.Entity("Integrado.Domain.Models.Microregion", b =>
                {
                    b.HasOne("Integrado.Domain.Models.HealthCenter", "HealthCenter")
                        .WithMany("Microregions")
                        .HasForeignKey("HealthCenterId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("HealthCenter");
                });

            modelBuilder.Entity("Integrado.Domain.Models.Person", b =>
                {
                    b.HasOne("Integrado.Domain.Models.Microregion", "Microregion")
                        .WithMany("Persons")
                        .HasForeignKey("MicroregionId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Microregion");
                });

            modelBuilder.Entity("Integrado.Domain.Models.QuestionAnswer", b =>
                {
                    b.HasOne("Integrado.Domain.Models.Question", "Question")
                        .WithMany("QuestionAnswers")
                        .HasForeignKey("QuestionId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Question");
                });

            modelBuilder.Entity("Integrado.Domain.Models.UserPermission", b =>
                {
                    b.HasOne("Integrado.Domain.Models.Permission", "Permission")
                        .WithMany("Users")
                        .HasForeignKey("PermissionId")
                        .OnDelete(DeleteBehavior.Restrict)
                        .IsRequired();

                    b.HasOne("Integrado.Domain.Models.User", "User")
                        .WithMany("Permissions")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Restrict)
                        .IsRequired();

                    b.Navigation("Permission");

                    b.Navigation("User");
                });

            modelBuilder.Entity("Integrado.Domain.Models.HealthCenter", b =>
                {
                    b.Navigation("Agents");

                    b.Navigation("Microregions");
                });

            modelBuilder.Entity("Integrado.Domain.Models.Microregion", b =>
                {
                    b.Navigation("Agents");

                    b.Navigation("Homes");

                    b.Navigation("Persons");
                });

            modelBuilder.Entity("Integrado.Domain.Models.Permission", b =>
                {
                    b.Navigation("Users");
                });

            modelBuilder.Entity("Integrado.Domain.Models.Person", b =>
                {
                    b.Navigation("Home")
                        .IsRequired();
                });

            modelBuilder.Entity("Integrado.Domain.Models.Question", b =>
                {
                    b.Navigation("ChecklistQuestions");

                    b.Navigation("QuestionAnswers");
                });

            modelBuilder.Entity("Integrado.Domain.Models.User", b =>
                {
                    b.Navigation("Agents");

                    b.Navigation("HealthCenter")
                        .IsRequired();

                    b.Navigation("Permissions");
                });
#pragma warning restore 612, 618
        }
    }
}
