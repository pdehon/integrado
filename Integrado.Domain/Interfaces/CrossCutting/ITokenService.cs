﻿using Integrado.Domain.Models;

namespace Integrado.Domain.Interfaces.CrossCutting
{
    public interface ITokenService
    {
        string GenerateJwtToken(User user);
    }
}
