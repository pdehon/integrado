﻿using Integrado.Domain.Core.Data;
using Integrado.Domain.Interfaces.CrossCutting;
using Integrado.Domain.Interfaces.Data;
using Integrado.Infra.CrossCutting.Mapper;
using Integrado.Infra.CrossCutting.Services;
using Integrado.Infra.Data.Context;
using Integrado.Infra.Data.Repositories;
using Microsoft.Extensions.DependencyInjection;


namespace Integrado.Infra.CrossCutting
{
    public static class NativeInjectorBootStrapper
    {
        public static void RegisterServices(IServiceCollection services)
        {
            RegisterData(services);
            RegisterServicesLayers(services);
        }

        private static void RegisterServicesLayers(IServiceCollection services)
        {
            services.AddScoped<IPasswordHashService, PasswordHashService>();
            services.AddScoped<ITokenService, TokenService>();
            var mappers = AutoMapperConfig.Setup();
            var temp = new List<Type>(mappers);
            services.AddAutoMapper(temp.ToArray());
        }

        private static void RegisterData(IServiceCollection services)
        {
            // Infra - Data
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IQuestionAnswerRepository, QuestionAnswerRepository>();
            services.AddScoped<IQuestionRepository, QuestionRepository>();
            services.AddScoped<IOtherQuestionRepository, OtherQuestionRepository>();
            services.AddScoped<IHomeRepository, HomeRepository>();
            services.AddScoped<IPersonRepository, PersonRepository>();
            services.AddScoped<IMicroregionRepository, MicroregionRepository>();
            services.AddScoped<IHealthCenterRepository, HealhCenterRepository>();
            services.AddScoped<IPermissionRepository, PermissionRepository>();
            services.AddScoped<IUserPermissionRepository, UserPermissionRepository>();
            services.AddScoped<IAgentRepository, AgentRepository>();
            services.AddScoped<IVisitRepository, VisitRepository>();
            services.AddScoped<IUnitOfWork, ApplicationContext>();
        }
    }
}