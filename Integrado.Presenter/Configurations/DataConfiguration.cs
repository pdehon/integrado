﻿using Integrado.Infra.Data.Context;

namespace Integrado.Presenter.Configurations
{
    public static class DataConfiguration
    {
        public static void AddDatabase(this IServiceCollection services)
        {
            services.AddDbContext<ApplicationContext>();
        }
    }
}