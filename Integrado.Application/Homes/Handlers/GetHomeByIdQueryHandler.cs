﻿using AutoMapper;
using Integrado.Application.Homes.Queries;
using Integrado.Application.Homes.Responses;
using Integrado.Application.Persons.Responses;
using Integrado.Domain.Interfaces.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Integrado.Application.Homes.Handlers
{
    public class GetHomeByIdQueryHandler : IRequestHandler<GetHomeByIdQuery, HomeResponse>
    {
        private readonly IHomeRepository _homeRepository;
        private readonly IPersonRepository _personRepository;
        private readonly IMapper _mapper;

        public GetHomeByIdQueryHandler(IHomeRepository homeRepository,
                                       IPersonRepository personRepository,
                                       IMapper mapper)
        {
            _homeRepository = homeRepository;
            _personRepository = personRepository;
            _mapper = mapper;
        }
        public async Task<HomeResponse> Handle(GetHomeByIdQuery request, CancellationToken cancellationToken)
        {
            var entity = await _homeRepository.GetByIdAsync(request.Id);
            if (entity is null)
                return null;
            
            var result = _mapper.Map<HomeResponse>(entity);

            var responsibleDocument = await _personRepository.Include().FirstOrDefaultAsync(p => p.HomeId == entity.Id && p.IsHeadFamily);
            if (responsibleDocument is not null)
                result.ResponsibleDocument = responsibleDocument.Document;

            return result;
        }
    }
}
