﻿using Integrado.Application.Visits.Responses;
using Integrado.Domain.Core.Messaging;

namespace Integrado.Application.Visits.Queries
{
    public class GetVisitByIdQuery : Query<VisitResponse>
    {
        public Guid Id { get; }

        public GetVisitByIdQuery(Guid id) => Id = id;
    }
}
