﻿using AutoMapper;
using Integrado.Application.Visits.Commands;
using Integrado.Application.Visits.Responses;
using Integrado.Domain.Models;

namespace Integrado.Application.Visits
{
    public class VisitMappingProfile : Profile
    {
        public VisitMappingProfile()
        {
            CreateMap<Visit, VisitResponse>();

            CreateMap<VisitAnswerCreateCommand, QuestionAnswer>();
        }
    }
}
