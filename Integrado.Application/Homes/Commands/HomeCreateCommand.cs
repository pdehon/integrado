﻿using Integrado.Application.Homes.Responses;
using Integrado.Domain.Core.Messaging;
using Integrado.Domain.Core.Responses;
using Integrado.Domain.ValueObjects;

namespace Integrado.Application.Homes.Commands
{
    public class HomeCreateCommand : Command<Response<HomeResponse>>
    {
        public Guid MicroregionId { get; private set; }
        public Address Address { get; private set; }
        public string ContactNumber { get; private set; }
        public string? MedicalRecordNumber { get; private set; }
        public decimal HouseholdIncome { get; private set; }
        public int NumberMembers { get; private set; }

        public HomeCreateCommand(Guid microregionId, Address address, string contactNumber, string? medicalRecordNumber, decimal householdIncome, int numberMembers)
        {
            MicroregionId = microregionId;
            Address = address;
            ContactNumber = contactNumber;
            MedicalRecordNumber = medicalRecordNumber;
            HouseholdIncome = householdIncome;
            NumberMembers = numberMembers;
        }
    }
}
