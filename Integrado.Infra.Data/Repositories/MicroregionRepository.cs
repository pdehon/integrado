﻿using Integrado.Domain.Interfaces.Data;
using Integrado.Domain.Models;
using Integrado.Infra.Data.Context;

namespace Integrado.Infra.Data.Repositories
{
    public class MicroregionRepository : Repository<Microregion>, IMicroregionRepository
    {
        public MicroregionRepository(ApplicationContext context)
            : base(context)
        {
        }
    }
}
