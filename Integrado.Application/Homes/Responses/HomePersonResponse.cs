﻿namespace Integrado.Application.Homes.Responses
{
    public class HomePersonResponse
    {
        public string Name { get; private set; }
        public string Document { get; private set; }
        public bool IsHeadFamily { get; private set; }
    }
}
