﻿using Integrado.Application.Persons.Commands;
using Integrado.Application.Persons.Handlers;
using Integrado.Application.Persons.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Integrado.Presenter.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonController : ApiController
    {
        private readonly IMediator _mediator;

        public PersonController(IMediator mediator) => _mediator = mediator;

        [HttpPost("create")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Create([FromBody] PersonCreateCommand command)
        {
            var response = await _mediator.Send(command);
            return CustomResponse(response);
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> Get([FromQuery] GetPersonListQuery query)
        {
            var response = await _mediator.Send(query);
            return CustomResponse(response);
        }

        [HttpGet("{id:Guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Get(Guid id)
        {
            var response = await _mediator.Send(new GetPersonByIdQuery(id));
            return CustomResponse(response);
        }

        [HttpPatch("{id:Guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Patch([FromBody] PersonUpdateCommand command)
        {
            var response = await _mediator.Send(command);
            return CustomResponse(response);
        }
    }
}