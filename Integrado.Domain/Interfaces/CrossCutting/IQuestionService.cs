﻿using Integrado.Domain.Models;

namespace Integrado.Domain.Interfaces.CrossCutting
{
    public interface IQuestionService
    {
        Task<List<Question>> GetAll();
    }
}
