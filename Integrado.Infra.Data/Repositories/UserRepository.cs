﻿using Integrado.Domain.Interfaces.Data;
using Integrado.Domain.Models;
using Integrado.Infra.Data.Context;

namespace Integrado.Infra.Data.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(ApplicationContext context)
            : base(context)
        {
        }
    }
}
