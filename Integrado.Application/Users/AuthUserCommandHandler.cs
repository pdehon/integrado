﻿using Integrado.Domain.Core.Data;
using Integrado.Domain.Core.Messaging;
using Integrado.Domain.Core.Responses;
using Integrado.Domain.Interfaces.CrossCutting;
using Integrado.Domain.Interfaces.Data;
using FluentValidation.Results;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Integrado.Application.Users
{
    public class AuthUserCommandHandler : CommandHandler, IRequestHandler<AuthUserCommand, Response<AuthUserResponse>?>
    {
        private readonly IUserRepository _userRepository;
        private readonly IPasswordHashService _passwordHashService;
        private readonly ITokenService _tokenService;

        public AuthUserCommandHandler(IUserRepository userRepository,
                                      IPasswordHashService passwordHashService,
                                      ITokenService tokenService,
                                      IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _userRepository = userRepository;
            _passwordHashService = passwordHashService;
            _tokenService = tokenService;
        }

        public async Task<Response<AuthUserResponse>?> Handle(AuthUserCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var user = await _userRepository.Include().Where(c => c.Email == request.Email).FirstOrDefaultAsync();
                if (user is null || !_passwordHashService.VerifyPassword(request.Password, user.PasswordHash))
                    return null;

                var token = _tokenService.GenerateJwtToken(user);

                return Success(new AuthUserResponse(user.Name, token, user.Id), new ValidationResult()) ;
            }
            catch (Exception ex)
            {
                AddError("Erro ao realizar autenticação: " + ex.Message);
                return Fail<AuthUserResponse>(ValidationResult);
            }
        }
    }
}
