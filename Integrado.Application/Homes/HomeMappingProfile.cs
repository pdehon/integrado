﻿using AutoMapper;
using Integrado.Application.Homes.Commands;
using Integrado.Application.Homes.Responses;
using Integrado.Domain.Homes;
using Integrado.Domain.Models;

namespace Integrado.Application.Homes
{
    public class HomeMappingProfile : Profile
    {
        public HomeMappingProfile()
        {
            CreateMap<Home, HomeResponse>()
                .ForMember(dest => dest.ResponsibleDocument, opt => opt.MapFrom(src => src.Persons.FirstOrDefault(p => p.IsHeadFamily).Document));
            CreateMap<Person, HomePersonResponse>();
            CreateMap<Microregion, HomeMicroregionResponse>();
            CreateMap<HomeUpdateCommand, Home>();
        }
    }
}
