﻿using Integrado.Domain.Core.Data;
using Integrado.Domain.Models;

namespace Integrado.Domain.Interfaces.Data
{
    public interface IUserRepository : IRepository<User>
    {
    }
}
