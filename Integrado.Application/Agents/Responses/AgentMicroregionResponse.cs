﻿namespace Integrado.Application.Agents.Responses
{
    public class AgentMicroregionResponse
    {
        public Guid Id { get; set; }
        public string? Name { get; set; }
    }
}
