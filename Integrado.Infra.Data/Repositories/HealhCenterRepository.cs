﻿using Integrado.Domain.Interfaces.Data;
using Integrado.Domain.Models;
using Integrado.Infra.Data.Context;

namespace Integrado.Infra.Data.Repositories
{
    public class HealhCenterRepository : Repository<HealthCenter>, IHealthCenterRepository
    {
        public HealhCenterRepository(ApplicationContext context)
            : base(context)
        {
        }
    }
}
