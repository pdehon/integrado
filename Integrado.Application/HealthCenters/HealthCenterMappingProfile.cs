﻿using AutoMapper;
using Integrado.Application.HealthCenters.Responses;
using Integrado.Domain.Models;

namespace Integrado.Application.HealthCenters
{
    public class HealthCenterMappingProfile : Profile
    {
        public HealthCenterMappingProfile()
        {
            CreateMap<HealthCenter, HealthCenterAgentResponse>();
        }
    }
}
