﻿using Integrado.Application.Visits.Responses;
using Integrado.Domain.Core.Messaging;
using Integrado.Domain.Core.Responses;

namespace Integrado.Application.Visits.Commands
{
    public class VisitAnswerCreateCommand : Command<Response<VisitResponse>>
    {
        public Guid QuestionId { get; set; }
        public string? Response { get; set; }
    }
}
