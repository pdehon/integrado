﻿using Integrado.Application.Homes.Responses;
using Integrado.Domain.Core.Messaging;

namespace Integrado.Application.Homes.Queries
{
    public class GetHomeByIdQuery : Query<HomeResponse>
    {
        public Guid Id { get; }

        public GetHomeByIdQuery(Guid id) => Id = id;
    }
}
