﻿using Integrado.Domain.Core.Options;

namespace Integrado.Presenter.Configurations
{
    public static class ApiConfiguration
    {
        public static void AddWebApi(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<JwtAppOptions>(configuration.GetSection("JwtApp"));
        }
    }
}