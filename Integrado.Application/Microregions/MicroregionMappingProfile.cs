﻿using AutoMapper;
using Integrado.Application.Microregions.Responses;
using Integrado.Domain.Models;

namespace Integrado.Application.Microregions
{
    public class MicroregionMappingProfile : Profile
    {
        public MicroregionMappingProfile()
        {
            CreateMap<Microregion, MicroregionResponse>();
        }
    }
}
