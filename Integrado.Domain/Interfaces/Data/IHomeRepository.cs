﻿using Integrado.Domain.Core.Data;
using Integrado.Domain.Homes;
using Integrado.Domain.Models;

namespace Integrado.Domain.Interfaces.Data
{
    public interface IHomeRepository : IRepository<Home>
    {
    }
}
