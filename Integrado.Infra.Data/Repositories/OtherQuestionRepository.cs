﻿using Integrado.Domain.Interfaces.Data;
using Integrado.Domain.Models;
using Integrado.Infra.Data.Context;

namespace Integrado.Infra.Data.Repositories
{
    public class OtherQuestionRepository : Repository<OtherQuestion>, IOtherQuestionRepository
    {
        public OtherQuestionRepository(ApplicationContext context)
            : base(context)
        {
        }
    }
}
