﻿using Integrado.Application.Homes.Responses;
using Integrado.Domain.Core.Messaging;
using Integrado.Domain.Core.Responses;
using Integrado.Domain.ValueObjects;

namespace Integrado.Application.Homes.Commands
{
    public class HomeUpdateCommand : Command<Response<HomeResponse>>
    {
        public Guid Id { get; private set; }
        public Guid MicroregionId { get; private set; }
        public Address Address { get; private set; }
        public string ContactNumber { get; private set; }
        public string? MedicalRecordNumber { get; private set; }
        public string? HeadFamilyDocument { get; private set; }
        public decimal HouseholdIncome { get; private set; }
        public int NumberMembers { get; private set; }

        public HomeUpdateCommand(Guid id, Guid microregionId, Address address, string contactNumber, string? medicalRecordNumber, string? headFamilyDocument, decimal householdIncome, int numberMembers)
        {
            Id = id;
            MicroregionId = microregionId;
            Address = address;
            ContactNumber = contactNumber;
            MedicalRecordNumber = medicalRecordNumber;
            HeadFamilyDocument = headFamilyDocument;
            HouseholdIncome = householdIncome;
            NumberMembers = numberMembers;
        }
    }
}
