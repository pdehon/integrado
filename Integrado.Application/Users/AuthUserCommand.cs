﻿using Integrado.Domain.Core.Messaging;
using Integrado.Domain.Core.Responses;

namespace Integrado.Application.Users
{
    public class AuthUserCommand : Command<Response<AuthUserResponse>>
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
