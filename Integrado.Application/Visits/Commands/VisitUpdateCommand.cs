﻿using Integrado.Application.Visits.Responses;
using Integrado.Domain.Core.Messaging;
using Integrado.Domain.Core.Responses;
using Integrado.Domain.Models;

namespace Integrado.Application.Visits.Commands
{
    public class VisitUpdateCommand : Command<Response<VisitResponse>>
    {
        public Guid Id { get; set; }
        public Guid AgentId { get; set; }
        public Guid PersonId { get; set; }
        public Guid HomeId { get; set; }
        public Guid MicroregionId { get; set; }
        public IEnumerable<QuestionAnswer> Answers { get; set; }
    }
}