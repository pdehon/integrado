﻿using Integrado.Domain.Homes;
using Integrado.Domain.Interfaces.Data;
using Integrado.Infra.Data.Context;

namespace Integrado.Infra.Data.Repositories
{
    public class HomeRepository : Repository<Home>, IHomeRepository
    {
        public HomeRepository(ApplicationContext context)
            : base(context)
        {
        }
    }
}
