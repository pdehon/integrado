﻿namespace Integrado.Application.HealthCenters.Responses
{
    public class HealthCenterAgentResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Cnes { get; set; }
    }
}
