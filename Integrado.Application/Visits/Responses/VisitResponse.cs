﻿namespace Integrado.Application.Visits.Responses
{
    public class VisitResponse
    {
        public Guid Id { get; set; }
    }
}
