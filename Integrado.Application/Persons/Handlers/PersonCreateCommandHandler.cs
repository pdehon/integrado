﻿using AutoMapper;
using Integrado.Application.Persons.Commands;
using Integrado.Application.Persons.Responses;
using Integrado.Domain.Core.Messaging;
using Integrado.Domain.Core.Responses;
using Integrado.Domain.Interfaces.Data;
using Integrado.Domain.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Integrado.Application.Persons.Handlers
{
    public class PersonCreateCommandHandler : CommandHandler, IRequestHandler<PersonCreateCommand, Response<PersonResponse>?>
    {
        private readonly IPersonRepository _personRepository;
        private readonly IHomeRepository _homeRepository;
        private readonly IMicroregionRepository _microregionRepository;
        private readonly IMapper _mapper;

        public PersonCreateCommandHandler(IPersonRepository personRepository,
                                        IHomeRepository homeRepository,
                                        IMicroregionRepository microregionRepository,
                                        IMapper mapper) : base(personRepository.UnitOfWork)
        {
            _personRepository = personRepository;
            _homeRepository = homeRepository;
            _microregionRepository = microregionRepository;
            _mapper = mapper;

        }

        public async Task<Response<PersonResponse>?> Handle(PersonCreateCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var home = await _homeRepository.Include().FirstOrDefaultAsync(c => c.Id == request.HomeId);
                if (home is null)
                {
                    AddError("Não foi encontrada o domicílio informado na base de dados");
                    return Fail<PersonResponse>(ValidationResult);
                }

                var microregion = await _microregionRepository.Include().FirstOrDefaultAsync(c => c.Id == request.MicroregionId);
                if (microregion is null)
                {
                    AddError("Não foi encontrada a microárea informada na base de dados");
                    return Fail<PersonResponse>(ValidationResult);
                }
                var person = new Person(request.Name,
                                        request.SocialName,
                                        request.BirthDate,
                                        request.Nationality,
                                        request.Sex,
                                        request.SkinColor,
                                        request.Document,
                                        request.DocumentType,
                                        request.Email,
                                        request.ContactNumber,
                                        request.SocialIdentification,
                                        request.FatherName,
                                        request.MotherName,
                                        request.IsHeadFamily,
                                        microregion.Id,
                                        home.Id);

                if (!await IsValidAsync(person))
                    return Fail<PersonResponse>(ValidationResult);

                var entity = await _personRepository.CreateAsync(person, cancellationToken);
                var result = await CommitAsync(cancellationToken);
                if (!result.IsValid)
                    return Fail<PersonResponse>(await RollbackAsync(cancellationToken));
                return Success(_mapper.Map<PersonResponse>(entity), result);
            }
            catch (Exception ex)
            {
                AddError("Erro ao realizar o cadastro de Indivíduo: " + ex.Message);
                return Fail<PersonResponse>(ValidationResult);
            }
        }
    }
}
