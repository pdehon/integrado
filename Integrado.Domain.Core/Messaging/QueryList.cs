﻿using MediatR;

namespace Integrado.Domain.Core.Messaging
{
    public class QueryList<TResponse> : IRequest<TResponse>
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string? Search { get; set; }
    }
}
