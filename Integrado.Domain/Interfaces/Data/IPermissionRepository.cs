﻿using Integrado.Domain.Core.Data;
using Integrado.Domain.Models;

namespace Integrado.Domain.Interfaces.Data
{
    public interface IPermissionRepository : IRepository<Permission>
    {
    }
}
