﻿namespace Integrado.Application.Persons.Responses
{
    public class PersonMicroregionResponse
    {
        public string? Name { get; set; }
    }
}
