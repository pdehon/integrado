﻿using MediatR;

namespace Integrado.Domain.Core.Messaging
{
    public class Query<TResponse> : IRequest<TResponse>
    {
    }
}