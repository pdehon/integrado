﻿using Integrado.Application.Visits.Responses;
using Integrado.Domain.Core.Messaging;

namespace Integrado.Application.Visits.Queries
{
    public class GetVisitListQuery : Query<IEnumerable<VisitResponse>>
    {
        public Guid MicroregionId { get; }
    }
}
