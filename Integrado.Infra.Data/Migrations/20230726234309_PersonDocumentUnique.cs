﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Integrado.Infra.Data.Migrations
{
    /// <inheritdoc />
    public partial class PersonDocumentUnique : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Person_Document",
                table: "Person",
                column: "Document",
                unique: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Person_Document",
                table: "Person");
        }
    }
}
