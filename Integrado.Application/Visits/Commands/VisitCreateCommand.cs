﻿using Integrado.Application.Visits.Responses;
using Integrado.Domain.Core.Messaging;
using Integrado.Domain.Core.Responses;

namespace Integrado.Application.Visits.Commands
{
    public class VisitCreateCommand : Command<Response<VisitResponse>>
    {
        public Guid AgentId { get; set; }
        public Guid PersonId { get; set; }
        public Guid HomeId { get; set; }
        public Guid MicroregionId { get; set; }
        public IEnumerable<VisitAnswerCreateCommand> Answers { get; set; }
    }
}
