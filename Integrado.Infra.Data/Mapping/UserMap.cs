﻿using Integrado.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Integrado.Infra.Data.Mapping
{
    internal class UserMap : EntityTypeConfiguration<User>
    {
        protected override void Configure(EntityTypeBuilder<User> builder)
        {
            builder.Property(v => v.Name)
                .HasColumnType("varchar(200)")
                .IsRequired();

            builder.Property(c => c.Email)
                .HasColumnType("varchar(50)")
                .IsRequired();

            builder.Property(c => c.PasswordHash)
                .HasColumnType("nvarchar(256)")
                .IsRequired();

        }
    }
}
