﻿using AutoMapper;
using Integrado.Application.Persons.Commands;
using Integrado.Application.Persons.Responses;
using Integrado.Domain.Homes;
using Integrado.Domain.Models;

namespace Integrado.Application.Persons
{
    public class PersonMappingProfile : Profile
    {
        public PersonMappingProfile()
        {
            CreateMap<Person, PersonResponse>()
                .ForMember(dest => dest.HomeId, opt => opt.MapFrom(src => src.HomeId));
            CreateMap<Microregion, PersonMicroregionResponse>();

            CreateMap<PersonCreateCommand, Person>();
        }
    }
}
