﻿namespace Integrado.Domain.Enum
{
    public enum EQuestionType
    {
        STRING = 0,
        NUMERIC = 1,
        DATE = 2,
        DROPLIST = 3,
        CHECKLIST = 4,
        RADIO = 5,
    }
}
