﻿using Integrado.Application.Agents.Responses;
using Integrado.Domain.Core.Messaging;

namespace Integrado.Application.Agents.Queries
{
    public class GetListAgentQuery : QueryList<IEnumerable<AgentResponse>>
    {
    }
}
