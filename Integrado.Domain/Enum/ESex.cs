﻿namespace Integrado.Domain.Enum
{
    public enum ESex
    {
        Feminino = 0,
        Masculino = 1,
    }
}
