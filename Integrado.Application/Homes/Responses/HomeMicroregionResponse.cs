﻿namespace Integrado.Application.Homes.Responses
{
    public class HomeMicroregionResponse
    {
        public Guid Id { get; set; }
        public string? Name { get; set; }
    }
}
