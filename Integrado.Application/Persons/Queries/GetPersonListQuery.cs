﻿using Integrado.Application.Persons.Responses;
using Integrado.Domain.Core.Messaging;

namespace Integrado.Application.Persons.Queries
{
    public class GetPersonListQuery : QueryList<IEnumerable<PersonResponse>?>
    {
        public Guid HomeId { get; set; }
    }
}
